# Laosdirg

Start with

```
cd public
npx live-server --port=1337 --entry-file=404.html
```

When deployed to gitlab, all requests are `404`'d and redirected to 404.html, which makes routing work.
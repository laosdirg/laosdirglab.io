import { h, render } from "./dependencies/preact.js";
import Application from "./Application.js";

const container = document.getElementById("root");
const root = h(Application, null);

// render component tree into dom
render(root, container);

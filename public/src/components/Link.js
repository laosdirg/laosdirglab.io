import { html } from "../dependencies/htm.js";

function Link(props) {
  return html`<a class="text-blue-500 hover:text-blue-800" ...${props} />`;
}

export default Link;

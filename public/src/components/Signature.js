import { html } from "../dependencies/htm.js";

const styles = {
  table: {
    width: 360,
    background: "none",
    border: 0,
    margin: 0,
    padding: 0,
  },
  name: {
    paddingBottom: 5,
    color: "#000000",
    fontSize: 18,
    fontFamily: "Arial, Helvetica, sans-serif",
  },
  title: {
    color: "#333333",
    fontSize: 14,
    fontFamily: "Arial, Helvetica, sans-serif",
    fontStyle: "italic",
  },
  companyName: {
    color: "#333333",
    fontSize: 14,
    fontFamily: "Arial, Helvetica, sans-serif",
    fontWeight: "bold",
  },
  letterLabel: {
    verticalAlign: "top",
    color: "#000000",
    fontSize: 14,
    fontFamily: "Arial, Helvetica, sans-serif",
    paddingRight: "5px",
  },
  value: {
    verticalAlign: "top",
    color: "#333333",
    fontSize: 14,
    fontFamily: "Arial, Helvetica, sans-serif",
    paddingRight: "10px",
  },
  link: {
    color: "#1da1db",
    textDecoration: "none",
    fontWeight: "normal",
    fontSize: 14,
  },
  linkedInCell: { paddingTop: 5 },
  linnkedInAnchor: { borderWidth: 0, border: 0, textDecoration: "none" },
  linkedInImage: {
    border: "none",
    width: 25,
    maxWidth: "25px !important",
    height: 25,
    maxHeight: "25px !important",
  },
  logo: {
    width: "80px",
    display: "inline",
  },
  logoTable: {
    textAlign: "right",
  },
};

function Signature({ name, phone, title, tld, inputRef, ...props }) {
  const email = `${name.split(" ")[0].toLowerCase()}@`;
  return html`
    <table
      ref=${inputRef}
      cellpadding=${0}
      cellspacing=${0}
      style=${styles.table}
    >
      <tbody>
        <tr>
          <td colspan=${4} style=${styles.name}>${name}</td>
        </tr>
        <tr>
          <td colspan=${3} style=${styles.title}>${title}</td>
          <td colspan=${3} rowspan=${4} style=${styles.logoTable}>
            <img
              style=${styles.logo}
              src="https://drive.google.com/uc?id=1fXzH8cjCUedB1BT4DpKdfUWBZI9tfpTl"
            />
          </td>
        </tr>
        <tr>
          <td colspan=${4} style=${styles.companyName}>Bytes & Brains</td>
        </tr>
        <tr>
          <td style=${styles.letterLabel}>m:</td>
          <td colspan=${3} style=${styles.value}>
            ${`+45 ${phone.substr(0, 4)} ${phone.substr(4)}`}
          </td>
        </tr>
        <tr>
          <td rowspan=${2} style=${styles.letterLabel}>a:</td>
          <td colspan=${3} style=${styles.value}>
            Ved Stadsgraven 9, kld. tv.
          </td>
        </tr>
        <tr>
          <td colspan=${3} style=${styles.value}>2300 København S</td>
        </tr>
        <tr>
          <td style=${styles.letterLabel}>w:</td>
          <td style=${styles.value}>
            <a
              href=${`http://bytesandbrains.${tld}?utm_source=mail_kommunikation&utm_medium=mail`}
              style=${styles.link}
            >
              bytesandbrains.${tld}
            </a>
          </td>
          <td style=${styles.letterLabel}>e:</td>
          <td style=${{ ...styles.value, paddingRight: 0 }}>
            <a href="mailto:${email}" style=${styles.link}>${email}bytesandbrains.${tld}</a>
          </td>
        </tr>
        <tr>
          <td colspan=${2} style=${styles.linkedInCell}>
            <a
              href="https://www.linkedin.com/company/bytesandbrains/"
              style=${styles.linkedInAnchor}
            >
              <img
                src="https://drive.google.com/uc?id=1xibLyq-u7SWsLOv7G2mu5Mnu0XptTHR_"
                style=${styles.linkedInImage}
              />
            </a>
          </td>
        </tr>
      </tbody>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <table
      ref=${inputRef}
      cellpadding=${0}
      cellspacing=${0}
      style=${styles.table}
    >
      <tbody>
        <tr>
          <td colspan=${4} style=${styles.name}>${name}</td>
        </tr>
        <tr>
          <td colspan=${3} style=${styles.title}>${title}</td>
          <td colspan=${3} rowspan=${4} style=${styles.logoTable}>
            <img
              style=${styles.logo}
              src="https://drive.google.com/uc?id=10JpG38O-kSeIJZXAehZwYduyK9kst-IA"
            />
          </td>
        </tr>
        <tr>
          <td colspan=${4} style=${styles.companyName}>Teal Medical</td>
        </tr>
        <tr>
          <td style=${styles.letterLabel}>m:</td>
          <td colspan=${3} style=${styles.value}>
            ${`+45 ${phone.substr(0, 4)} ${phone.substr(4)}`}
          </td>
        </tr>
        <tr>
          <td rowspan=${2} style=${styles.letterLabel}>a:</td>
          <td colspan=${3} style=${styles.value}>
            Ved Stadsgraven 9, kld. tv.
          </td>
        </tr>
        <tr>
          <td colspan=${3} style=${styles.value}>2300 København S</td>
        </tr>
        <tr>
          <td style=${styles.letterLabel}>w:</td>
          <td style=${styles.value}>
            <a
              href=${`https://tealmedical.io/`}
              style=${styles.link}
            >
              tealmedical.io
            </a>
          </td>
          <td style=${styles.letterLabel}>e:</td>
          <td style=${{ ...styles.value, paddingRight: 0 }}>
            <a href="mailto:${email}" style=${styles.link}>${email}tealmedical.io</a>
          </td>
        </tr>
        <tr>
          <td colspan=${2} style=${styles.linkedInCell}>
            <a
              href="https://www.linkedin.com/company/teal-medical/"
              style=${styles.linkedInAnchor}
            >
              <img
                src="https://drive.google.com/uc?id=1xibLyq-u7SWsLOv7G2mu5Mnu0XptTHR_"
                style=${styles.linkedInImage}
              />
            </a>
          </td>
        </tr>
      </tbody>
    </table>
  `;
}

export default Signature;

import Router from "./dependencies/preact-router.js";
import { html } from "./dependencies/htm.js";
import SignaturePage from "./pages/SignaturePage.js";
import EmailSetupPage from "./pages/EmailSetupPage.js";
import HomePage from "./pages/HomePage.js";

// the root component, it just handles routing
function Application() {
  return html`
    <${Router}>
      <${HomePage} path="/" />
      <${EmailSetupPage} path="/email" />
      <${SignaturePage} path="/signature" />
    <//>
  `;
}

export default Application;

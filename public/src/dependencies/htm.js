import htm from "https://cdn.skypack.dev/htm@^3.0.4";

// re-export bound template function
import { h } from "./preact.js";
const html = htm.bind(h);
export { html };

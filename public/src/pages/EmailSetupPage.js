import { html } from "../dependencies/htm.js";
import Link from "../components/Link.js";

function EmailSetupPage() {
  return html`
    <article class="container mx-auto p-5">
      <h1 class="text-3xl font-bold">Setting up your company email</h1>

      You already own all email addresses corresponding to your username for all
      of our domains (john@bytesandbrains.com, john@laosdirg.com, etc.).
      However, additional steps are required for:

      <ul class="list-disc list-inside mt-2">
        <li>
          <${Link} href="#adding-additional-aliases"
            >adding additional aliases<//
          >,
        </li>
        <li>
          <${Link} href="#sending-from-your-alias">sending from your alias<//>,
        </li>
        <li>
          ${"and "}
          <${Link} href="#setting-up-a-signature">setting up a signature<//>.
        </li>
      </ul>

      <section id="adding-additional-aliases" class="mt-6">
        <h2 class="text-2xl font-bold">Adding additional aliases</h2>

        We are billed per user, however you can add aliases free of charge.
        Incoming email to any of your aliases is put into your${" "}
        <kbd>@laosdirg.com</kbd> inbox.

        <ol class="list-decimal list-inside">
          <li>
            Sign in to${" "}
            <${Link} href="https://admin.google.com/">G Suite administration<//
            >,
          </li>
          <li>
            go to the${" "}
            <${Link} href="https://admin.google.com/u/2/ac/users">User List<//>,
            <img src="/images/users.png" alt="the Users button" />
          </li>
          <li>
            (possibly opt-in to the new dashboard,)
            <img src="/images/new_version.png" alt="opt-in button" />
          </li>
          <li>
            select yourself,
            <img src="/images/yourself.png" alt="a beautiful face" />
          </li>
          <li>
            click anywhere on <em>User information</em>,
            <img src="/images/user_info.png" alt="user information" />
          </li>
          <li>
            scroll down to and click anywhere on <em>Email aliases</em>,
            <img src="/images/aliases.png" alt="aliases button" />
          </li>
          <li>
            type in your desired alias(es),
            <img src="/images/new_alias.png" alt="new alias" />
          </li>
          <li>
            scroll down and click <em>SAVE</em>.
            <img src="/images/save.png" alt="save" />
          </li>
        </ol>

        After a few minutes, aliases for all domains will automatically be
        generated.
      </section>

      <section id="sending-from-your-alias" class="mt-6">
        <h2 class="text-2xl font-bold">Sending from your alias</h2>

        In order to use your alias(es) for outgoing email you will have to add
        them in your email client(s). The following instructions are for Gmail
        (which is also how you setup Inbox).
        <ol>
          <li>
            Go to <em>Settings</em>,
            <img src="/images/alias1.png" alt="Add alias" />
          </li>

          <li>
            after selecting the <em>Accounts</em> tab, within the
            <em>Send mail as</em> box, click <em>Add another email address</em>,
            <img src="/images/alias2.png" alt="Add alias" />
          </li>
          <li>
            enter the email you wish to send from, and click <em>Next Step</em>.
            <img src="/images/alias3.png" alt="Add alias" />
          </li>
          <li>
            (Optionally set a new default address.)
            <img src="/images/default.png" alt="Add alias" />
          </li>
        </ol>
        Repeat for all addresses you wish to send from.
        <em>
          Note: Any aliases you add within Gmail will also be available in
          Inbox:
        </em>
        <img src="/images/inbox.png" alt="Inbox" />
      </section>

      <section id="setting-up-a-signature" class="mt-6">
        <h2 class="text-2xl font-bold">Setting up a signature</h2>

        <${Link} href="/signature">Use Emils tool<//>
      </section>
    </article>
  `;
}

export default EmailSetupPage;

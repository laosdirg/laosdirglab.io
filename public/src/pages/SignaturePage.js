import { useState, useRef } from "../dependencies/preact.js";
import { html } from "../dependencies/htm.js";
import Signature from "../components/Signature.js";

function SignatureForm() {
  const [state, setState] = useState({
    name: "",
    title: "",
    phone: "",
    tld: "dk",
  });
  const inputRef = useRef();

  function handleChange(event) {
    const { name, value } = event.currentTarget;
    switch (name) {
      case "name":
        setState((state) => ({
          ...state,
          name: value,
        }));
        break;
      case "title":
        setState((state) => ({
          ...state,
          title: value,
        }));
        break;
      case "phone":
        setState((state) => ({
          ...state,
          phone: value,
        }));
        break;
      case "tld":
        setState((state) => ({
          ...state,
          tld: value,
        }));
        break;
    }
  }

  function handleSubmit(event) {
    event.preventDefault();
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNode(inputRef.current);
    selection.addRange(range);
    try {
      var successful = document.execCommand("copy");
      var msg = successful ? "successful" : "unsuccessful";
      console.log("Copying text command was " + msg);
    } catch (err) {
      console.log("Oops, unable to copy");
    }
  }

  return html`
    <div>
      <div class="signatureform">
        <form onSubmit=${handleSubmit}>
          <label>
            Navn:
            <input
              name="name"
              type="text"
              value=${state.name}
              onInput=${handleChange}
            />
          </label>
          <label>
            Titel:
            <input
              name="title"
              type="text"
              value=${state.title}
              onInput=${handleChange}
            />
          </label>
          <label>
            Mobil:
            <input
              name="phone"
              type="text"
              value=${state.phone}
              onInput=${handleChange}
            />
          </label>
          <label>
            .dk:
            <input
              name="tld"
              type="radio"
              value="dk"
              checked=${state.tld === "dk"}
              onInput=${handleChange}
            />
          </label>
          <label>
            .com:
            <input
              name="tld"
              type="radio"
              value="com"
              checked=${state.tld === "com"}
              onInput=${handleChange}
            />
          </label>
          <button type="submit">Copy</button>
        </form>
      </div>
      <br />
      <br />
      <br />
      <${Signature}
        inputRef=${inputRef}
        name=${state.name}
        title=${state.title}
        phone=${state.phone}
        tld=${state.tld}
      />
    </div>
  `;
}

export default SignatureForm;

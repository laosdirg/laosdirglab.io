import { html } from "../dependencies/htm.js";
import Link from "../components/Link.js";

function HomePage() {
  return html`
    <div class="flex flex-col items-center justify-center h-screen">
      <p>Looking for</p>
      <p>the <${Link} href="/email">Email setup guide<//>?</p>
    </div>
  `;
}

export default HomePage;
